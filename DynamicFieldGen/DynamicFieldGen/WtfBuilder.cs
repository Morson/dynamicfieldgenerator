﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace DynamicFieldGen
{
    public class WtfBuilder
    {
        private Type _gtype;
        public void AssignWhatTypeWeWantToWtf(Type t)
        {
            _gtype = t;
        }
 
        private Dictionary<string, Type> GetWtfFields()
        {
            var t = _gtype.GetFields();
            var dic = new Dictionary<string, Type>();
            for (int i = 0; i < t.Length; i++)
            {
                dic.Add(t[i].Name+"1", t[i].FieldType);
                dic.Add(t[i].Name + "2", t[i].FieldType);
                //TODO:generalnie koncepcje nazewnicza dublowanych pol trzeba by poprawic
            }
 
            return dic;
        }
 
        private string GetWtfName()
        {
            return "doubled_"+_gtype.Name;
        }
 
        public void WtfWtfWtf()
        {
            AssemblyName wtfName = new AssemblyName("wtf");
            AssemblyBuilder ab =
                AppDomain.CurrentDomain.DefineDynamicAssembly(
                    wtfName,
                    AssemblyBuilderAccess.RunAndSave);
 
            ModuleBuilder mb =
                ab.DefineDynamicModule(wtfName.Name, wtfName.Name + ".dll");
 
            TypeBuilder tb = mb.DefineType(GetWtfName(),
                TypeAttributes.Public);
 
            foreach (var item in GetWtfFields())
            {
                tb.DefineField(
                    item.Key,
                    item.Value,
                    FieldAttributes.Public);
            }
 
            Type t = tb.CreateType();
 
            ab.Save(wtfName.Name + ".dll");
            //wewnatrz wygenerowanej dllki mozna obejrzec wygenerowana klase ... do podejrzenia dllki ja uzywam dotpeeka ew. reflector ilspy jak se stryjenka zawinszuje
 
            object wtfUfokurwaleci = Activator.CreateInstance(t);
            //można se stworzyć i zastanowić się ,przyjrzeć pomyśleć jakie to kurwa brzydkie i potem ... nie wiem
        }
    }
}